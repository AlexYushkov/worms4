﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class WormScriptAnimation : MonoBehaviour
{
   [SerializeField] private Sprite[] SpriteAnimations;
   [SerializeField] private SpriteRenderer renderer;
   
   private void Start()
   {
      Invoke(nameof(FillWorm), 1.1f);
      renderer.sprite = SpriteAnimations[0];
      StartCoroutine(animation());
   }

   private void FillWorm()
   {
      renderer.DOFade(1, 0.7f);
   }

   private IEnumerator animation()
   {
      int pos = 0;
      while (true)
      {
         pos++;
         
         yield return new WaitForSeconds(0.1f);
         renderer.sprite = SpriteAnimations[pos];
         pos = Checking(pos, SpriteAnimations.Length - 1);
      }
   }

   private int Checking(int pos, int lenght)
   {
      return pos >= lenght? 0 : pos;
   }
}
