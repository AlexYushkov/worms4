﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PreGamePreviewDialog : MonoBehaviour
{
    [SerializeField] private Image WormPreview;
    [SerializeField] private Button Wormprev;
    [SerializeField] private Button Wormnext;
    
    [SerializeField] private Image MapPreview;
    [SerializeField] private Button Mapprev;
    [SerializeField] private Button Mapnext;

    [SerializeField] private GameObject NameFiled; 
    [SerializeField] private Button StartGame;
    
    private void Start()
    {
        Setup();
    }

    private void Setup()
    {
        GetComponent<Image>().DOFade(0.25f, 0.4f).OnComplete(() =>
        {
            WormPreview.DOFade(1f, 0.7f);
            MapPreview.DOFade(1f, 0.7f);
            
            NameFiled.transform.DOLocalMoveY(235, 0.7f).OnComplete(() =>
            {
                StartGame.GetComponent<Image>().DOFade(1, 2);

                Wormprev.GetComponent<Image>().DOFade(1, 0.5f).OnComplete(() =>
                {
                    Wormnext.GetComponent<Image>().DOFade(1, 0.5f).OnComplete(() =>
                    {
                        Mapprev.GetComponent<Image>().DOFade(1, 0.5f).OnComplete(() =>
                            {
                                Mapnext.GetComponent<Image>().DOFade(1, 0.5f);
                            });
                    });
                }); 
            }); 
        });

        StartGame.onClick.AddListener(() =>
        {
            PlayerPrefs.SetInt("Worm_color", FindObjectOfType<ChangeColor>().GetColorIndex());
            PlayerPrefs.SetInt("Worm_map", FindObjectOfType<MapChangeScript>().GetMapIndex());

            string name = NameFiled.GetComponent<InputField>().text.Equals("")
                ? "PlayersWorm"
                : NameFiled.GetComponent<InputField>().text;
            PlayerPrefs.SetString("Worm_name", NameFiled.GetComponent<InputField>().text);
            SceneManager.LoadScene("polygon");
        });
    }
}
