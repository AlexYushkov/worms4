﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class MeteorScript : MonoBehaviour
{
    [SerializeField] private GameObject mp;
    [SerializeField] private Vector2[] _vector2s;
    [SerializeField] private bool isStoper;

    void Start()
    {
        StartCoroutine(MeteorRain());
    }

    private IEnumerator MeteorRain()
    {
        yield return new WaitForSeconds(Random.Range(1, 3));

        for (int i = 3; i >= 1; i--)
        {
            var r = Instantiate(mp, transform);
            var vectorMet = Random.Range(0, _vector2s.Length);

            switch (Random.Range(0, _vector2s.Length)) 
            {
                case 0: 
                    r.transform.DORotate(new Vector3(0f, 0f, -64f), 0);
                    break;
                case 2:
                    r.transform.DORotate(new Vector3(0f, 0f, -100f), 0);
                    break;
            }
            
            r.GetComponent<Rigidbody2D>().AddForce(_vector2s[vectorMet], ForceMode2D.Impulse);
            yield return new WaitForSeconds(Random.Range(2, 5));
        }
        
        if(isStoper && PlayerPrefs.GetInt("PlayerDeath") == 0)
            FindObjectOfType<TimerForTurn>().StopTimer();
        Destroy(gameObject);
    }
}
