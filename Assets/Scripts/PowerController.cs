﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class PowerController : MonoBehaviour
{
    [SerializeField] private SpriteRenderer[] _power;

    public void SetPowerStage(int power)
    {
        StartCoroutine(powerStage(power));
    }

    private IEnumerator powerStage(int power)
    {
        for (int i = 0; i < power; i++)
        {
            _power[i].DOFade(1, 1);
            yield return new WaitForSeconds(1);
        }
        
        yield return new WaitForSeconds(2);
        _power[0].DOFade(0, 0);
        _power[1].DOFade(0, 0);
        _power[2].DOFade(0, 0);
        _power[3].DOFade(0, 0);
    }
}
