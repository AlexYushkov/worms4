﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using GoogleMobileAds.Api;
using UnityEngine;
using UnityEngine.UI;

public class MainSceneController : MonoBehaviour
{
    [SerializeField] private GameObject PreGamePreview;
    [SerializeField] private GameObject logo;
    [SerializeField] private Button PlayGame;
    private int MapIndex = 0;
    private string WormName = "Player";

    private void Awake()
    {
        MobileAds.Initialize(status => {} );
    }

    private void Start()
    {
        if(PlayGame != null)
            PlayGame.onClick.AddListener(ShowPreGamePreview);
        if(logo != null)
            logo.transform.DOLocalMove(new Vector3(0.07f, -2.8f, -266.7461f), 1).SetDelay(1.5f).SetEase(Ease.InOutCirc);
    }

    private void ShowPreGamePreview()    
    {
        PlayGame.GetComponent<Image>().DOFade(0, 0.5f);
        var d = Instantiate(PreGamePreview, transform);
        d.GetComponent<RectTransform>().sizeDelta = GetComponent<RectTransform>().sizeDelta;
        
        var t = d.GetComponent<RectTransform>();
        t.localScale = new Vector3(1, 1, 1);
        t.localPosition = new Vector3(0, 0, 0);
    }
}
