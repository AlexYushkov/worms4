﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class G_Control : MonoBehaviour
{  
   [Header("Colors")]
   [SerializeField] private Color[] _colors;
   [Header("Other")]
   [SerializeField] private GameObject PlayerGameController;
   
   private List<MapVisitors> queue1 = new List<MapVisitors>();
   private bool isDeleted;
   private int INX = 0;

   private static int enemyCount = 2;
   private void Awake()
   {
      PlayerPrefs.SetInt("PlayerDeath", 0);
      int inx = 0;
      var _queue = FindObjectsOfType<MapVisitors>();

      foreach (var Q in _queue)
      {
         if (Q.gameObject.transform.CompareTag("Player"))
         {
            queue1.Insert(0, Q);
         }
         else
         {
            queue1.Add(Q);
         }
      }
      InitilizeColors();
   }

   private void InitilizeColors()
   {
      Names n = new Names(new []{"A", "B", "C", "D", "E", "F", "G", "H"});
      List<int> used = new List<int>();
      
      queue1[0].GetComponent<SpriteRenderer>().color = _colors[PlayerPrefs.GetInt("Worm_color")];
      queue1[0].GetComponent<MapVisitors>().SetColor(ColorNames(PlayerPrefs.GetInt("Worm_color")));
      queue1[0].GetComponent<PlayerController>().GetPlayerName().text = PlayerPrefs.GetString("Worm_name");
      
      used.Add(PlayerPrefs.GetInt("Worm_color"));
      
      for (int i = 1; i < queue1.Count; i++)
      {
         bool exit = false;
         while (!exit)
         {
            int c = Random.Range(0, _colors.Length);
            if (!used.Contains(c))
            {
               exit = true;
               used.Add(c);
               queue1[i].GetComponent<SpriteRenderer>().color = _colors[c];
               
               queue1[i].GetComponent<MapVisitors>().SetColor(ColorNames(c));
               queue1[i].GetComponent<MapVisitors>().SetName("Bot:" + n.GetName(c));
            }
         }
      }
      
   }

   private string ColorNames(int inx)
   {
      switch (inx)
      {
         case 0: return "white";
         case 1: return "red";
         case 2: return "green";
         case 3: return "pink";
         case 4: return "yellow";
         case 5: return "blue";
         case 6: return "navy-blue";
         case 7: return "purple";
      }

      return "";
   }

   private void Start()
   {
      Invoke(nameof(SetupStep), 2);
   }

   public void NextTurn()
   {
      INX++;
      INX = CheckInx();
      Invoke(nameof(SetupStep), 3);
   }

   private int CheckInx()
   {
      return INX >= queue1.Count ? 0 : INX;
   }

   private void SetupStep()
   {
      string who = "";
      if (queue1[INX].name.Equals("worms")) {
         PlayerGameController.SetActive(true);
         who = "player";
      }
      else {
         PlayerGameController.SetActive(false);
         queue1[INX].GetComponent<BotBehavior>().Step();
         who = "bot";
      }
      FindObjectOfType<TimerForTurn>().StartTimer(who, queue1[INX].GetComponent<MapVisitors>().GetColor());
   }

   public string ReturnPlayer()
   {
      return queue1[INX].name;
   }

   public static int getEnemyCount()
   {
      return enemyCount;
   }

   public static void setEnemyCount()
   {
      enemyCount -= 1;
   }

   public void deleteEnemyLink(MapVisitors enemyToDelete)
   {
      foreach (var item in queue1)
      {
         if (enemyToDelete == item)
         {
            queue1.Remove(item);
            break;
         }
      }
   }

   public class Names
   {
      private string[] names;

      public Names(string[] names)
      {
         this.names = names;
      }

      public string GetName(int inx)
      {
         return names[inx];
      }
   }
}
