﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapVisitors : MonoBehaviour
{
    private string Color = "";
    private string Name = "";

    public void SetColor(string color)
    {
        Color = color;
    }
    
    public void SetName(string name)
    {
        Name = name;
    }
    
    public string GetColor()
    {
        return Color;
    }
    
    public string GetName()
    {
        return Name;
    }
}
