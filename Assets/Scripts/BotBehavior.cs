﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class BotBehavior : MonoBehaviour
{
	[SerializeField] private Sprite[] SpriteAnimations;
	[SerializeField] private Sprite[] DamageSpriteAnimation;
	
	[SerializeField] private TextMeshProUGUI hp;
	[SerializeField] private TextMeshProUGUI NAME;
	
	[SerializeField] private SpriteRenderer renderer;
	[SerializeField] private SpriteRenderer cross;
	
	[SerializeField] private GameObject crossParent;
	[SerializeField] private GameObject FirePos;
	[SerializeField] private GameObject BazukaPref;
	[SerializeField] private GameObject Donkey;
	[SerializeField] private GameObject Meteor;
	[SerializeField] private GameObject winP;
	
	[SerializeField] private PowerController powerController;
	
	private int TurnSecnods = 0;
	private int power;
	private int minPower = 3;
	private int _HP = 100;
	private int Direction = 0;
	
	private bool WayRight = true;
	private bool destroyed = false;
	private bool killed = false;
	private bool AlivePlayer = true;
	
	private Rigidbody2D _rigidbody2D;
	private GameObject player;
	private MapVisitors mapVisitors;
	private G_Control globalControl;
	
	void Start()
	{
		_rigidbody2D = GetComponent<Rigidbody2D>();
		mapVisitors = GetComponent<MapVisitors>();
		globalControl = FindObjectOfType<G_Control>();//GetComponent<G_Control>();
		
		player = GameObject.FindGameObjectWithTag("Player");
		NAME.text = GetComponent<MapVisitors>().GetName();
	}
	
	public void Step()
	{
		if(PlayerPrefs.GetInt("PlayerDeath") == 0)
			Setup(player.transform.position.x > transform.position.x);
	}

	private void Setup(bool s)
    {
	    TurnSecnods = Random.Range(2, 5);
	    switch (s)
	    {
		    case false:
			    Direction = 2;
			    break;
		    case true:
			    Flip();
			    Direction = -2;
			    break;
	    }
	    
	    Movement();
    }

	private int Checking(int pos, int lenght)
    {
	    return pos >= lenght? 0 : pos;
    }
	
    private void Movement()
    {
	    if (TurnSecnods >= 1)
	    {
		    _rigidbody2D.velocity = new Vector2(Direction, _rigidbody2D.velocity.y);
		    TurnSecnods--;
		    StartCoroutine(Animation(SpriteAnimations, 0.05f));
		    Invoke(nameof(Movement), 0.5f);
	    }
	    else
	    {
		    Flip();
			RandomWeapon();
	    }
    }

    private void RandomWeapon()
    {
	    switch (Random.Range(1,4))
	    {
		    case 1: Invoke(nameof(Aim), 0.5f); break;
		    case 2: Invoke(nameof(CallDonkey), 0.5f); break;
		    case 3: Invoke(nameof(CallMeteor), 0.5f); break;
	    }
    }
    
    private void CallMeteor()
    {
	    Instantiate(Meteor);
    }
    
    private void CallDonkey()
    {
	    var d = Instantiate(Donkey);
	    d.transform.position = new Vector3(GameObject.FindGameObjectWithTag("Player").transform.position.x, Donkey.transform.position.y, Donkey.transform.position.z);
    }

    private void Aim()
    {
	    cross.DOFade(1, 0.8f).OnComplete(() =>
	    {
		    int angle = GameObject.FindGameObjectWithTag("Player").transform.position.x > transform.position.x
			    ? -30
			    : 30;
		    crossParent.transform.DOLocalRotate(new Vector3(0, 0, angle), 1.5f);

		    power = Random.Range(1, 5);

		    if (Vector3.Distance(GameObject.FindGameObjectWithTag("Player").transform.position, transform.position) >= 6)
		    {
			    power = 4;
		    }
		    powerController.SetPowerStage(power);
		    Invoke(nameof(BazukaDelay), power);
	    });
    }

    private void BazukaDelay()
    {
	    cross.gameObject.SetActive(false);
	    
	    var heading = FirePos.transform.position - transform.position;
	    var direction = heading / heading.magnitude;
	    
	    ShootBazuka(direction);
    }

    private void ShootBazuka(Vector3 v)
    {
	    var b = Instantiate(BazukaPref, FirePos.transform);
	    b.GetComponent<Rigidbody2D>().AddForce(v*CalcShootPower(), ForceMode2D.Impulse);
	    FindObjectOfType<TimerForTurn>().StopTimer();
    }

    private int CalcShootPower()
    {
	    return  minPower * power;
    }

    void Flip()
	{
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}


    private void OnCollisionEnter2D(Collision2D other)
    {
	    if (other.collider.gameObject.tag.Equals("Ammo"))
	    {
		    Damage();
		    var heading = transform.position - other.collider.gameObject.transform.position;
		    var direction = heading / heading.magnitude;
		    
		    _rigidbody2D.AddForce(direction*-2, ForceMode2D.Force);
		    StartCoroutine(Animation(DamageSpriteAnimation, 0.07f));
	    }

	    if (other.collider.gameObject.tag.Equals("Death"))
	    {
		    Destroy();
	    }
    }

    private void Damage()
    {
	    _HP -= 10;
	    if (_HP <= 0)
	    {
		    G_Control.setEnemyCount();
		    globalControl.deleteEnemyLink(mapVisitors);
		    
		    ShowWinPanel();
		    Destroy(gameObject);
	    }
	    hp.text = "" + _HP;
    }
    
    private void Destroy()
    {
	    destroyed = true;
	    G_Control.setEnemyCount();
	    
	    FindObjectOfType<TimerForTurn>().StopTimer();
	    globalControl.deleteEnemyLink(mapVisitors);
	    ShowWinPanel();
	    Destroy(gameObject);	
    }
    
    private IEnumerator Animation(Sprite[] animation, float CoroutineTime)
    {
	    int pos = 0;
	    for (int i = 0; i < animation.Length + 1; i++)
	    {
		    pos++;
		    yield return new WaitForSeconds(CoroutineTime);
		    renderer.sprite = animation[pos];
            
		    pos = Checking(pos, animation.Length - 1);
	    } 
    }

    private void ShowWinPanel()
    {
	    if (G_Control.getEnemyCount() == 0)
	    {
		    winP.SetActive(true);
	    }
    }
}