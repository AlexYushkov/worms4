﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

public class AimController : MonoBehaviour
{
    private GameConntroller GC;
    private GameObject player;
    private GameObject pref;

    private void Awake()
    {
        GC = FindObjectOfType<GameConntroller>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    private void Update()
    {
        transform.position = GC.GetFingerPosition();
    }

    public void Destr(GameObject firepos)
    {
        var Ptr = player.transform.position;
        var Etr = transform.position;
        var heading = Etr - Ptr;
        var dist = heading.magnitude;
        var dir = heading / dist;
        
        FindObjectOfType<ShootScript>().Shoot(dir, firepos.transform);
        Destroy(gameObject);
    }
}
