﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeColor : MonoBehaviour
{
    [SerializeField] private Color[] _colors;
    [SerializeField] private SpriteRenderer _renderer;
    
    [SerializeField] private Button Next;
    [SerializeField] private Button Prev;
    private int ColorIndex = 0;

    private void Start()
    {
        _renderer.color = _colors[0];
        Next.onClick.AddListener(NextColor);
        Prev.onClick.AddListener(PrevColor);
    }

    private void NextColor()
    {
        ColorIndex++;
        ColorIndex = Checking();
        _renderer.color = _colors[ColorIndex];
    }

    private void PrevColor()
    {
        ColorIndex--;
        ColorIndex = CheckingPrefCount();
        _renderer.color = _colors[ColorIndex];
    }

    private int Checking()
    {
        if (ColorIndex > 0)
        {
            if (ColorIndex >= _colors.Length)
            {
                return 0;
            }
            return ColorIndex;
        }
        return _colors.Length-1;
    }

    private int CheckingPrefCount()
    {
        if (ColorIndex < 0)
        {
            return _colors.Length - 1;
        }
        return ColorIndex;
    }
    
    
    public int GetColorIndex()
    {
        return ColorIndex;
    }
}
