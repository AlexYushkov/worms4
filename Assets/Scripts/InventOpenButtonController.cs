﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InventOpenButtonController : MonoBehaviour
{
    [SerializeField] private Button bazuka, meteor, donkey;
    [SerializeField] private Button closeInv;
    [SerializeField] private GameObject selected_first, selected_second, selected_third;
    
    private void Start()
    {
        setActiveElements(false, false, false);
        bazuka.onClick.AddListener(() =>
        {
            PlayerPrefs.SetInt("Worms_weapon", 1);
            setActiveElements(true, false, false);
        });
        meteor.onClick.AddListener(() =>
        {
            PlayerPrefs.SetInt("Worms_weapon", 2);
            setActiveElements(false, true, false);
        });
        donkey.onClick.AddListener(() =>
        {
            PlayerPrefs.SetInt("Worms_weapon", 3);
            setActiveElements(false, false, true);
            
        });
        closeInv.onClick.AddListener(() =>
        {
            gameObject.SetActive(false);
        });
    }

    private void setActiveElements(bool first, bool second, bool third)
    {
        selected_first.SetActive(first);
        selected_second.SetActive(second);
        selected_third.SetActive(third);
    }
}