﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LosePanelControler : MonoBehaviour
{
    [SerializeField] private Transform gameOver;
    [SerializeField] private Button ads, menu;
    [SerializeField] private TextMeshProUGUI text1;
    [SerializeField] private TextMeshProUGUI text2;
   
    void Start()
    {
        gameOver.DOScale(new Vector3(9, 9, 9), 4f).OnComplete(() =>
        {
            var localPosition = gameOver.localPosition;
            
            gameOver.DOLocalMove(new Vector3(localPosition.x, localPosition.y + 200,
                    localPosition.z), 2f).OnComplete(() =>
                {
                    ads.GetComponent<Image>().DOFade(1, 8f).OnComplete(() =>
                        {
                            ads.transform.DOShakePosition(4, new Vector3(10, 10, 0))
                                .SetLoops(-1, LoopType.Restart);
                        });
                    menu.GetComponent<Image>().DOFade(1, 8f);
                    text1.DOFade(1, 8f);
                    text2.DOFade(1, 8f);
                });
        });
        
        ads.onClick.AddListener(() => Advertisment());

        menu.onClick.AddListener(() => { SceneManager.LoadScene("mainScene"); });
    }
    private void Advertisment()
    {
        GetComponent<TestAds>().Ads(FindObjectOfType<PlayerController>().GetRewardedAction());
    }
}
