﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ShootScript : MonoBehaviour
{
    [SerializeField] private GameObject bomb;
    [SerializeField] private GameObject Donkey;
    [SerializeField] private GameObject Meteor;
    public void Shoot(Vector3 v, Transform firePos)
    {
        var weapon = PlayerPrefs.GetInt("Worms_weapon");
        PlayerPrefs.SetInt("PlayerShooted", 1);
        
        UnityAction FireAction = weapon == 1 ? GrenadeThrown(firePos, v) : weapon == 2 ? MeteorRainUse() : DonkeyUse();
        FireAction.Invoke();
    }

    private UnityAction GrenadeThrown(Transform FirePosition, Vector3 _vector)
    {
        return () =>
        {
            var b = Instantiate(bomb, FirePosition.transform);
            b.GetComponent<Rigidbody2D>().AddForce(_vector * 10, ForceMode2D.Impulse);
            FindObjectOfType<TimerForTurn>().StopTimer();  
        };
    }

    private UnityAction MeteorRainUse()
    {
        return () =>
        {
            Instantiate(Meteor);    
        };
    }

    private UnityAction DonkeyUse()
    {
        return () =>
        {
            var d = Instantiate(Donkey);
            var position = Donkey.transform.position;
            d.transform.position = new Vector3(GameObject.FindGameObjectWithTag("Cross").transform.position.x, position.y, position.z);
        };
    }
}
