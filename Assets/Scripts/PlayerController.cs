﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private GameObject NameObj;
    [SerializeField] private GameObject HpObj;
    [SerializeField] private GameObject respawn;
    [SerializeField] private GameObject loseP;

    [SerializeField] private Sprite[] DamageSpriteAnimation;
    [SerializeField] private Sprite[] SpriteAnimations;
    [SerializeField] private Sprite[] JumpSpriteAnim;
    
    [SerializeField] private SpriteRenderer renderer;

    [SerializeField] private TextMeshProUGUI nameTX;
    [SerializeField] private TextMeshProUGUI hpTX;
    
    private int hpCount = 100;
    private bool destroyed = false;
    private bool isRight = true;
    private bool isGround = false;
    private bool TurnForward = true;
    private float speed = 3f;
    private Rigidbody2D _rigidbody2D;
    
    void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }

    private void Destroy()
    {
        hpCount = 0;
        hpTX.text = "" + hpCount;
        ShowLosePanel();
    }

    public TextMeshProUGUI GetPlayerName()
    {
        return nameTX;
    }
    
    
    public void Walk(float side)
    {
        StartCoroutine(Animation(SpriteAnimations, 0.1f));
        _rigidbody2D.velocity = new Vector2(speed * side, _rigidbody2D.velocity.y);
    }

    public void Jump()
    {
        StartCoroutine(Animation(JumpSpriteAnim, 0.05f));
        _rigidbody2D.AddForce(Vector2.up * 0.5f, ForceMode2D.Impulse);
    }
    
    
    public void Spin(bool isLeft)
    {
        UnityAction Rotate = isLeft ? rotate(180) : rotate(0);
        Rotate.Invoke();
    }

    private UnityAction rotate (int angle)
    {
        return () =>
        {
            NameObj.transform.DOLocalRotate(new Vector3(0, angle, 0), 0);  
            HpObj.transform.DOLocalRotate(new Vector3(0, angle, 0), 0);  
            
            gameObject.transform.DOLocalRotate(new Vector3(0, angle, 0), 0);  
        };
    }

    public void SetIsRight(bool isRight)
    {
        this.isRight = isRight;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.gameObject.tag.Equals("Ammo"))
        {
            Damage();
            
            var vec = (transform.position - other.collider.gameObject.transform.position);
            var direction = vec / vec.magnitude;
            
            _rigidbody2D.AddForce(direction*-2, ForceMode2D.Force);
            StartCoroutine(Animation(DamageSpriteAnimation, 0.1f));
        }

        if (other.collider.gameObject.tag.Equals("Death"))
        {
            Destroy();   
        }
    }

    private void Damage()
    {
        hpCount -= 10;
        if (hpCount <= 0)
        {
            ShowLosePanel();
            return;
        }
        hpTX.text = "" + hpCount;
    }
    
    private IEnumerator Animation(Sprite[] animation, float CoroutineTime)
    {
        int pos = 0;
        for (int i = 0; i < animation.Length + 1; i++)
        {
            pos++;
            yield return new WaitForSeconds(CoroutineTime);
            renderer.sprite = animation[pos];
            
            pos = Checking(pos, animation.Length - 1);
        } 
    }

    private int Checking(int pos, int lenght)
    {
        return pos >= lenght? 0 : pos;
    }
    
    private void ShowLosePanel()
    {
        PlayerPrefs.SetInt("PlayerDeath", 1);
        loseP.SetActive(true);
    }

    public UnityAction GetRewardedAction()
    {
        return () =>
        {
            gameObject.transform.position = respawn.transform.position;
            hpCount = 50;
            hpTX.text = "" + hpCount;
            loseP.SetActive(false);
            FindObjectOfType<TimerForTurn>().StopTimer();
        };
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.collider.gameObject.CompareTag("Ground"))
        {
            isGround = false;
        }
    }
}
