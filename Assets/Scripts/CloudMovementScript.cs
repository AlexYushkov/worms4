﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class CloudMovementScript : MonoBehaviour
{
   [SerializeField] private float _duration = 60;
   [SerializeField] private Ease _type = Ease.Linear;
   
   private void Start()
   {
      GetComponent<SpriteRenderer>().DOFade(1, 6f);
      transform.DOMoveX(transform.position.x + (45), _duration).SetEase(_type).SetLoops(-1, LoopType.Restart).OnComplete(
         () =>
         {
            GetComponent<SpriteRenderer>().DOKill();
            GetComponent<SpriteRenderer>().DOFade(0, 0f);
            GetComponent<SpriteRenderer>().DOFade(1, 6f);
         });
   }
}
