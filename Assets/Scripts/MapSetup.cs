﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapSetup : MonoBehaviour
{
    [SerializeField] private MapType _mapType;

    private void Awake()
    {
        int mapInx = PlayerPrefs.GetInt("Worm_map");
        var map = _mapType.Get(mapInx);

        Instantiate(map.part1, transform);
        Instantiate(map.part2, transform);
        Instantiate(map.part3, transform);
        Instantiate(map.part4, transform);
        Instantiate(map.part5, transform);
    }
}
