﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapChangeScript : MonoBehaviour
{
    [SerializeField] private Sprite[] maps;
    [SerializeField] private Image _renderer;
    
    [SerializeField] private Button Next;
    [SerializeField] private Button Prev;
    private int MapIndex = 0;
    private void Start()
    {
        Next.onClick.AddListener(() =>NextColor());
        Prev.onClick.AddListener(()=> PrevColor());
    }

    private void NextColor()
    {
        MapIndex++;
        MapIndex = Checking();
        _renderer.sprite = maps[MapIndex];
    }

    private void PrevColor()
    {
        MapIndex--;
        MapIndex = CheckingPrefCount();
        _renderer.sprite = maps[MapIndex];
    }

    private int Checking()
    {
        if (MapIndex > 0)
        {
            if (MapIndex >= maps.Length)
            {
                return 0;
            }
            return MapIndex;
        }
        return maps.Length-1;
    }

    private int CheckingPrefCount()
    {
        if (MapIndex < 0)
        {
            return maps.Length - 1;
        }

        return MapIndex;
    }
    
    public int GetMapIndex()
    {
        return MapIndex;
    }
}
