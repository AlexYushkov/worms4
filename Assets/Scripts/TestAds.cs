﻿using System;
using System.Collections;
using System.Collections.Generic;
using GoogleMobileAds.Api;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TestAds : MonoBehaviour
{
    private RewardedAd rewardedAd;
    [SerializeField] private GameObject player;
    
    private UnityAction callback;
    
    void Start()
    {
        string id = "ca-app-pub-3940256099942544/5224354917";
        rewardedAd = new RewardedAd(id);
        
        this.rewardedAd.OnAdLoaded += HandleRewardedAdLoaded;
        
        this.rewardedAd.OnAdFailedToLoad += HandleRewardedAdFailedToLoad;
        
        this.rewardedAd.OnAdOpening += HandleRewardedAdOpening;
        
        this.rewardedAd.OnAdFailedToShow += HandleRewardedAdFailedToShow;
        
        this.rewardedAd.OnUserEarnedReward += HandleUserEarnedReward;
        
        this.rewardedAd.OnAdClosed += HandleRewardedAdClosed;

        
        AdRequest request = new AdRequest.Builder().Build();
        
        this.rewardedAd.LoadAd(request);
    }
    

    public void Ads(UnityAction Callback)
    {
        callback = Callback;
        if (rewardedAd.IsLoaded())
        {
            Debug.Log("ShowAd");
            rewardedAd.Show();
        }
    }

    public void HandleRewardedAdLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardedAdLoaded event received");
    }

    public void HandleRewardedAdFailedToLoad(object sender, AdErrorEventArgs args)
    {
        MonoBehaviour.print(
            "HandleRewardedAdFailedToLoad event received with message: "
                             + args.Message);
    }

    public void HandleRewardedAdOpening(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardedAdOpening event received");
    }

    public void HandleRewardedAdFailedToShow(object sender, AdErrorEventArgs args)
    {
        MonoBehaviour.print(
            "HandleRewardedAdFailedToShow event received with message: "
                             + args.Message);
    }

    public void HandleRewardedAdClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardedAdClosed event received");
        SceneManager.LoadScene("mainScene");
    }

    public void HandleUserEarnedReward(object sender, Reward args)
    {
        Debug.Log("REWARD");
        player.SetActive(true);
        callback.Invoke();
    }
    
    public void RespawnAfterAds()
    {
        PlayerPrefs.SetInt("Ads", 1);
    }
}
