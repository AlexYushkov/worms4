﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseController : MonoBehaviour
{
    [SerializeField] private GameObject music;
    [SerializeField] private Button resume;
    [SerializeField] private Sprite onMusic, offMusic;

    private bool isMusicOff;
    void Start()
    {
        music.GetComponent<Button>().onClick.AddListener(() =>
        {
            if (isMusicOff)
            {
                AudioListener.volume = 1;
                music.GetComponent<Image>().sprite = onMusic;
                isMusicOff = !isMusicOff;
            }
            else if (!isMusicOff)
            {
                AudioListener.volume = 0;
                music.GetComponent<Image>().sprite = offMusic;
            }
        });
        
        resume.onClick.AddListener(() =>
        {
            Time.timeScale = 1;
            gameObject.SetActive(false);
        });
    }
}
