﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TimerForTurn : MonoBehaviour
{
    [SerializeField] private GameObject seconds;
    [SerializeField] private GameObject whichTurn;
    [SerializeField] private GameObject image;

    private TextMeshProUGUI time;
    private TextMeshProUGUI tWhichTurn;
    private SpriteRenderer front;

    private int sec;
    
    void Start()
    {
        time = seconds.GetComponent<TextMeshProUGUI>();
        tWhichTurn = whichTurn.GetComponent<TextMeshProUGUI>();
        front = image.GetComponent<SpriteRenderer>();
    }
    
    public void StartTimer(string who, string color)
    {
        if (who.Equals("player"))
        {
            tWhichTurn.text = "Ваш ход, команда: " + color;
            StartCoroutine(nameof(timer));
        }
        else
        {
            tWhichTurn.text = "Ход врага, команда: " + color;
            StartCoroutine(nameof(timerbot));
        }
    }

    public void StopTimer()
    {
        if (FindObjectOfType<G_Control>().ReturnPlayer().Equals("worms"))
        {
            StopSec();
            StopCoroutine(nameof(timer));
            time.text = "";
        }
        else
            StopCoroutine(nameof(timerbot));
        
        FindObjectOfType<G_Control>().NextTurn();
    }

    private IEnumerator timerbot()
    {
        while (true)
        {
            time.text = ".";
            yield return new WaitForSeconds(1);
            time.text = "..";
            yield return new WaitForSeconds(1);
            time.text = "...";
            yield return new WaitForSeconds(1);
        }
    }

    public void StopSec()
    {
        sec = 0;
    }

    private IEnumerator timer()
    {
        PlayerPrefs.SetInt("PlayerShooted", 0);
        sec = 15;
        while (true)
        {
            sec = sec - 1;
            front.DOFade( 0, 15).Restart();
            if (sec == -1)
            {
                /*time.text = "" + 15;
                break;*/
                break;
            }
            
            time.text = "" + sec;
            yield return new WaitForSeconds(1);
        }
        setColor();
        time.text = "";
        
        if (PlayerPrefs.GetInt("PlayerShooted") == 0)
        {
            FindObjectOfType<G_Control>().NextTurn();
        }

        yield return new WaitForSeconds(1);
    }

    private void setColor()
    {
        front.DOKill();
        front.DOFade(1, 1f);
    }
}