﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WinPanelController : MonoBehaviour
{
    [SerializeField] private GameObject image;
    [SerializeField] private GameObject toMenu;
    [SerializeField] private TextMeshProUGUI text;
    
    void Start()
    {
        image.transform.DOScale(new Vector3(7, 7, 7), 4f).OnComplete(() =>
            {
                toMenu.GetComponent<Image>().DOFade(1, 3f);
                text.DOFade(1, 3f);
            });
        toMenu.GetComponent<Button>().onClick.AddListener(() => { SceneManager.LoadScene("mainScene"); });
    }
}
