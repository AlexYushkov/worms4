﻿using System;
using System.Collections;
using System.Collections.Generic;
using Destructible2D;
using UnityEngine;

public class DonkeyScript : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.gameObject.tag.Equals("Death"))
        {
            if(PlayerPrefs.GetInt("PlayerDeath") == 0)
                FindObjectOfType<TimerForTurn>().StopTimer();
            Destroy(gameObject);
        }
    }
}
