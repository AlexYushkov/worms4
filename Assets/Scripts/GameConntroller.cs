﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Timers;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

public class GameConntroller : MonoBehaviour, IPointerUpHandler, IDragHandler, IPointerClickHandler
{

    [SerializeField] private PlayerController _playerController; 
    
    [SerializeField] private GameObject firePos;
    [SerializeField] private GameObject aimPref;
    [SerializeField] private GameObject invent;
    [SerializeField] private GameObject pausePanel;
    
    [SerializeField] private Button openInv;
    [SerializeField] private Button pause;
    
    [SerializeField] private Camera cam;
    
    private float timeStartClick = 0;
    private float timeForDoubleClick = 1.0f;
    private Vector3 FingerPosition = Vector3.one;

    void Start()
    {
        PlayerPrefs.SetInt("Worms_weapon", 0);
        
        openInv.onClick.AddListener(() =>
        {
            invent.SetActive(true);
        });
        
        pause.onClick.AddListener(() =>
        {
            pausePanel.SetActive(true);
            Time.timeScale = 0;
        });
    }
    
    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            timeStartClick += Time.deltaTime;
        }
        else 
            timeStartClick = 0;

        if (timeStartClick >= 2f)
        {
            timeStartClick = 0;
            initAim();
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (FindObjectOfType<AimController>() != null)
        {
            FindObjectOfType<AimController>().Destr(firePos);    
        }
    }
    private void initAim()
    {
        if (FindObjectOfType<AimController>() == null)
        {
            Instantiate(aimPref, firePos.transform.position, aimPref.transform.rotation);
        }
    }
    
    public void OnDrag(PointerEventData eventData)
    {
        FingerPosition = eventData.pointerCurrentRaycast.worldPosition;
    }

    public ref Vector3 GetFingerPosition()
    {
        return ref FingerPosition;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.clickCount == 1)
        {
            var pos = cam.ScreenToWorldPoint(eventData.pressPosition);
            if (pos.x > _playerController.gameObject.transform.position.x)
            {
                _playerController.Spin(false);
                _playerController.SetIsRight(true);
                _playerController.Walk(1);
            }
            else
            {
                _playerController.Spin(true);
                _playerController.SetIsRight(false);
                _playerController.Walk(-1);
            }
        }

        else if (eventData.clickCount == 2)
        {
            _playerController.Jump();
        }
    }
}
