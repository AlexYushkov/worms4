﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MapType", menuName = "MapTypeSetup")]
public class MapType : ScriptableObject
{
    [SerializeField] private List<Map> Maps;

    public Map Get(int index)
    {
        return Maps.Find(x => x.MapIndex == index);
    }
}

[System.Serializable]
public struct Map
{
    public int MapIndex;
    public GameObject part1;
    public GameObject part2;
    public GameObject part3;
    public GameObject part4;
    public GameObject part5;
    
}